
package ontologies;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Andrei
 */
public class OrderTest {
  
    
    public OrderTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }
    
    @Before
    public void setUp() {
       
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getTraderID method, of class Order.
     */
    @Test
    public void testGetTraderID() {
        System.out.println("getTraderID");
        Order instance = new Order();
        instance.setTraderID("xd");
        String expResult = "xd";
        String result = instance.getTraderID();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of getVolume method, of class Order.
     */
    @Test
    public void testGetVolume() {
        System.out.println("getVolume");
        Order instance = new Order();
        instance.setVolume(40);
        int expResult = 40;
        int result = instance.getVolume();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of getOrderID method, of class Order.
     */
    @Test
    public void testGetOrderID() {
        System.out.println("getOrderID");
        Order instance = new Order();
        instance.setOrderID("B3");
        String expResult = "B3";
        String result = instance.getOrderID();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of getOrderType method, of class Order.
     */
    @Test
    public void testGetOrderType() {
        System.out.println("getOrderType");
        Order instance = new Order();
        instance.setOrderType("Limit");
        String expResult = "Limit";
        String result = instance.getOrderType();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of getPrice method, of class Order.
     */
    @Test
    public void testGetPrice() {
        System.out.println("getPrice");
        Order instance = new Order();
        instance.setPrice(30);
        int expResult = 30;
        int result = instance.getPrice();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of getSide method, of class Order.
     */
    @Test
    public void testGetSide() {
        System.out.println("getSide");
        Order instance = new Order();
        instance.setSide("Buy");
        String expResult = "Buy";
        String result = instance.getSide();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of getStatus method, of class Order.
     */
    @Test
    public void testGetStatus() {
        System.out.println("getStatus");
        Order instance = new Order();
        instance.setStatus("NEW");
        String expResult = "NEW";
        String result = instance.getStatus();
        assertEquals(expResult, result);
       
    }
}
