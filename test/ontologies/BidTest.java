/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ontologies;

import java.util.ArrayList;
import java.util.Arrays;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Andrei
 */
public class BidTest {
    private Order order1;
    
 
    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }
    
    @Before
    public void setUp() {
        Order order1 = new Order();
        order1.setOrderID("B1");
        order1.setOrderType("limit");
        order1.setPrice(40);
        order1.setSide("Buy");
        order1.setStatus("New");
        order1.setTraderID("Trader1");
        order1.setVolume(300);
    }
    
    @After
    public void tearDown() {
        order1 = null;
    }

    /**
     * Test of getPrice method, of class Bid.
     */
    @Test
    public void testGetPrice() {
        System.out.println("getPrice");
        Bid instance = new Bid();
        instance.setPrice(30);
        int expResult = 30;
        int result = instance.getPrice();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getBidOrders method, of class Bid.
     */
    @Test
    public void testGetBidOrders() {
        System.out.println("getBidOrders");
        Bid instance = new Bid();
        instance.setBidOrders(new ArrayList<Order>(Arrays.asList(order1)));
        ArrayList result = instance.getBidOrders();
        assertNotNull(result);   
    }

}
