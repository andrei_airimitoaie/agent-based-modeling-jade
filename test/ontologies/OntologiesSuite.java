/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ontologies;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author Andrei
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({ontologies.LimitOrderBookOntologyTest.class, ontologies.OfferTest.class, ontologies.MarketVocabularyTest.class, ontologies.BidTest.class, ontologies.BidsSortTest.class, ontologies.OffersSortTest.class, ontologies.OrderTest.class, ontologies.CancelOrderTest.class, ontologies.PlaceOrderTest.class})
public class OntologiesSuite {

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
    
}
