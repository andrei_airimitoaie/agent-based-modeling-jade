
package ontologies;

import java.util.ArrayList;
import java.util.Arrays;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Andrei
 */
public class OfferTest {
    private Order order1;
    
    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }
    
    @Before
    public void setUp() {
        Order order1 = new Order();
        order1.setOrderID("S1");
        order1.setOrderType("market");
        order1.setPrice(32);
        order1.setSide("Sell");
        order1.setStatus("New");
        order1.setTraderID("Trader3");
        order1.setVolume(300);
    }
    
    @After
    public void tearDown() {
        order1 = null;
    }

    /**
     * Test of getPrice method, of class Offer.
     */
    @Test
    public void testGetPrice() {
        System.out.println("getPrice");
        Offer instance = new Offer();
        instance.setPrice(20);
        int expResult = 20;
        int result = instance.getPrice();
        assertEquals(expResult, result);
    }

    /**
     * Test of getOfferOrders method, of class Offer.
     */
    @Test
    public void testGetOfferOrders() {
        System.out.println("getOfferOrders");
        Offer instance = new Offer();
        instance.setOfferOrders(new ArrayList<Order>(Arrays.asList(order1)));
        ArrayList result = instance.getOfferOrders();
        assertNotNull(result);
        
    }

}
