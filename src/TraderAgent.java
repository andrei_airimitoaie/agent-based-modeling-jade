
import jade.content.lang.Codec;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.SequentialBehaviour;
import jade.core.behaviours.SimpleBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;
import ontologies.*;
import ontologies.Order;

/**Title: Trader Agent
 * Description: This class implements a trader agent. 
 * Version:1.0
 * Last date modified: 30/11/2011
 * @author Andrei
 */
public class TraderAgent extends Agent implements MarketVocabulary{
    private Codec codec = new SLCodec();
    private Ontology ontology = LimitOrderBookOntology.getInstance();
    private String action;
    private AID server;
    private final int WAIT = -1;
    private final int QUIT = 0;
    private int command = WAIT;
    private int orderIDCounter = 1;
    private ArrayList<Order> orders = new ArrayList();
    
            
    
    @Override
    protected void setup(){
        System.out.println("Trader Agent "+getAID().getName()+" has been created");
        //Register language and ontology
        getContentManager().registerLanguage(codec);
        getContentManager().registerOntology(ontology);
        
        //Set the main behaviour of the Trader Agent
        SequentialBehaviour sb = new SequentialBehaviour();
        sb.addSubBehaviour(new GetAction(this));
        addBehaviour(sb);   
    }
    
    private class GetAction extends OneShotBehaviour{
        public GetAction(Agent a){
            super(a);
            command = WAIT;
        }
        @Override
        public void action(){
           try{
                command = getChoice();
                if(command == QUIT){
                System.out.println(getLocalName()+ " is shutting down!");
                doDelete();
                }
                else if (command == NEW_ORDER){
                        addBehaviour (new NewOrder(myAgent));
                }
                else if (command == CANCEL_ORDER){
                        addBehaviour(new Cancel(myAgent));
                }
                else{
                    System.out.println("Not a valid command !");
                }
            
        }catch (Exception ex) {ex.printStackTrace();}
        
        }
}

    private class NewOrder extends OneShotBehaviour{
        NewOrder(Agent a){
            super(a);
        }
        
        @Override
        public void action(){
              
            try{
                 int price = 0;
                 Scanner scanner = new Scanner(System.in);
                 System.out.print("Side:");
                 String side = scanner.nextLine();
                 System.out.print("Type:");
                 String type = scanner.nextLine();
                 if(type.equalsIgnoreCase("limit")){
                 System.out.print("Price: ");
                 price = scanner.nextInt();
                 }
                 System.out.print("Volume: ");
                 int volume = scanner.nextInt();
                 
                 PlaceOrder po = new PlaceOrder();
                 po.setTraderID(myAgent.getName());
                 po.setOrderID(Integer.toString(orderIDCounter));
                 po.setOrderType(type);
                 po.setSide(side);
                 if(type.equalsIgnoreCase("limit")){
                    po.setPrice(price);
                 }
                 po.setVolume(volume);
                 orderIDCounter++;
                 sendMessage(ACLMessage.REQUEST,po);
            }
            catch (Exception ex){ex.printStackTrace();}
        }
    }

    private class Cancel extends OneShotBehaviour{
        Cancel(Agent a){
            super(a);
        }
        @Override
        public void action() {
           try{
               Scanner scanner = new Scanner(System.in);
               System.out.println("Enter the ID of the order you wish to cancel: ");
               String id = scanner.nextLine();
               CancelOrder co = new CancelOrder();
               co.setOrderID(id);
               sendMessage(ACLMessage.REQUEST,co);
           }
           catch(Exception ex){ex.printStackTrace();}
        }
        
    }
  
    public void sendMessage(int performative, Object content) {
      if (server == null) lookupServer();
      if (server == null) {
         System.out.println("Unable to localize the server! \nOperation aborted!");
         return;
      }
      ACLMessage msg = new ACLMessage(performative);
      msg.setLanguage(codec.getName());
      msg.setOntology(ontology.getName());
      try {
         msg.setContentObject((java.io.Serializable)content);
         msg.addReceiver(server);
         send(msg);
      }
      catch (Exception ex) { ex.printStackTrace(); }
   }
    public   void lookupServer() {
        ServiceDescription sd = new ServiceDescription();
        sd.setType(SERVER_AGENT);
	DFAgentDescription dfd = new DFAgentDescription();
        dfd.addServices(sd);
        try {
	     DFAgentDescription[] dfds = DFService.search(this, dfd);
            if (dfds.length > 0 ) {
		    server = dfds[0].getName();
                    System.out.println("Localized server");
            }
            else  System.out.println("Couldn't localize server!");
        }
        catch (Exception ex) {
	     ex.printStackTrace();
             System.out.println("Failed searching int the DF!");
	}
   }
    
   private class ReceiveResponse extends SimpleBehaviour{
       private boolean finished = false;
       
       ReceiveResponse(Agent a){
           super(a);
       }
       
        @Override
       public void action(){
           ACLMessage msg = receive(MessageTemplate.MatchSender(server));
       //    if (msg == null ){ block(); return;}
           
           if (msg.getPerformative() == ACLMessage.REFUSE){
               System.out.println("Response from server: Rejected !");
           }
           else{
               try{
                   Object content = msg.getContent();
                   if (content instanceof Order){
                       Order order = (Order) content;
                       orders.add(order);
                       System.out.println(orders.get(0).getPrice());
                   }
                   else if(content instanceof CancelOrder){
                       CancelOrder cancel = (CancelOrder) content;
                       orders.remove(content);
                   }
               }
               catch (Exception ex){ex.printStackTrace();}
           }
       }
       
        @Override
       public boolean done(){
           return finished;
       }
   }
   
    
    public int getChoice(){
        System.out.print("\n\t<<---- SUBMIT ORDER - MENU ---->>" +
                       "\n\n\t0. Terminate agent" +
                       "\n\t1. Place an Order\n\t2. Cancel an Order" +
                       "\n\t3. Get the list of Orders\n> ");
        try {
            BufferedReader buf = new BufferedReader(new InputStreamReader(System.in));
            String in = buf.readLine();
            return Integer.parseInt(in);
	 }
	  catch (Exception ex) { ex.printStackTrace(); }
	  return 0;
   } 
    
}