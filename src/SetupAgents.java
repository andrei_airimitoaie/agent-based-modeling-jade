
import jade.core.Agent;
import jade.wrapper.AgentController;


/**Title: SetupAgents
 * Description: This class will start several market agents
 * Version: 1.0
 * Last date modified: 30/11/2011
 * @author Andrei
 */
public class SetupAgents extends Agent{
    private String agent1Name;
    private String agent2Name;
    private String agent3Name;
    private String agent4Name;
    Object [] args1 = new Object[5];
    Object [] args2 = new Object[5];
    Object [] args3 = new Object[5];
    Object [] args4 = new Object[5];
    
    public SetupAgents(){
        agent1Name = "Agent1";
        agent2Name = "Agent2";
        agent3Name = "Agent3";
        agent4Name = "Agent4";
    }
    
    public void setup(){
        jade.wrapper.AgentContainer c = getContainerController();
        args1[0]= "PlaceBid";
        args2[0]= "PlaceOffer";
        args3[0]= "PlaceBid";
        args4[0]= "PlaceOffer";
        args1[1]= agent1Name;
        args2[1]= agent2Name;
        args3[1]= agent3Name;
        args4[1]= agent4Name;
        args1[2]= "1";
        args2[2]= "2";
        args3[2]= "3";
        args4[2]= "4";
        args1[3]= "50";
        args2[3]= "50";
        args3[3]= "51";
        args4[3]= "49";
        args1[4]= "120";
        args2[4]= "2130";
        args3[4]= "503";
        args4[4]= "50312";
        try{
            AgentController agent1 = c.createNewAgent(agent1Name, "TraderAgent", args1);
            agent1.start();
            AgentController agent2 = c.createNewAgent(agent2Name, "TraderAgent", args2);
            agent2.start();
            AgentController agent3 = c.createNewAgent(agent3Name, "TraderAgent", args3);
            agent3.start();
            AgentController agent4 = c.createNewAgent(agent4Name, "TraderAgent", args4);
            agent4.start();
        }
        catch (Exception e){e.printStackTrace();}
        
        
    }
    
}
