
import java.util.ArrayList;
import ontologies.MarketVocabulary;
import ontologies.Offer;
import ontologies.Order;


/**Title: Matching Bid Orders
 * Description: This class will match bid orders against their counter parties
 * Version: 1.0
 * Last date modified: 05/12/2011
 * @author Andrei
 */
public class MatchBidOrders implements MarketVocabulary {
    Order bidOrder;
    ArrayList<Offer> offers;
    
    public MatchBidOrders(Order bidOrder, ArrayList<Offer> offers){
        this.bidOrder = bidOrder;
        this.offers = offers;
    }
    
    public void matchOrders(Order order, ArrayList<Offer> list){
        int price = order.getPrice();
        int volume = order.getVolume();
        int remainingVolume = volume;
        if (list.isEmpty()){
            System.out.println("Couldn't find any matching orders. The order "+ order.getOrderID()+" will remain on the Bids list !");
        }
        else if (list.get(0).getPrice() > price){
            System.out.println("Couldn't find any matching orders. The order "+ order.getOrderID()+" will remain on the Bids list !");
        }
        else{
            for(Offer offer:list){
                while(volume > 0 && !(list.isEmpty())){
                    if((volume - offer.getOfferOrders().get(0).getVolume()) >= 0){
                        volume = volume - offer.getOfferOrders().get(0).getVolume();
                        remainingVolume = volume;
                        System.out.println(remainingVolume);
                        offer.getOfferOrders().remove(0);
                    }
                    else{
                        int volumeLeftInOrder = offer.getOfferOrders().get(0).getVolume() - volume;
                        remainingVolume = 0;
                        offer.getOfferOrders().get(0).setVolume(volumeLeftInOrder);
                        System.out.println(volumeLeftInOrder);
                        break;
                    }
                }
            }
        }
    }
}
