package ontologies;
import jade.content.Concept;
import jade.content.onto.annotations.Slot;

/**Title: Order
 * Description: This class implements an order.
 * Version: 1.2
 * Last date modified: 01/11/2012
 * @author Andrei
 */
public class Order implements Concept{
    
    private String traderID;
    private String orderID;
    private String side;
    private String orderType;
    private int price;
    private int volume;
    private String status;
    
    public String getTraderID(){
        return traderID;
    }
    
    public int getVolume(){ 
        return volume;
    }
    
    public String getOrderID(){
        return orderID;
    }
    
    public void setTraderID(String traderID){
        this.traderID = traderID;
    }
    
    public void setOrderID(String orderID){
        this.orderID = orderID;
    }
    
    public void setVolume(int volume){
        this.volume = volume;
    }
    
    public String getOrderType(){
        return orderType;
    }
    
    public void setOrderType(String orderType){
        this.orderType = orderType;
    }
    
    @Slot (mandatory = false)
    public int getPrice(){
        return price;
    }
    @Slot (mandatory = false)
    public void setPrice(int price){
        this.price = price;
    }
    
    public String getSide(){
        return side;
    }
    
    public void setSide(String side){
        this.side = side;
    }
    
    @Slot (mandatory = false)
    public String getStatus(){
        return status;
    }
    
    @Slot (mandatory = false)
    public void setStatus(String status){
        this.status = status;
    }
    
}