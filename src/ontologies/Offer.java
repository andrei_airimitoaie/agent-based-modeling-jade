package ontologies;
import java.util.ArrayList;
import jade.content.Concept;

/**Name: Offer
 * Description: This class will create an offer. The offer will have 2 parameters : Price and a queue of Orders
 * Version: 1.1
 * Last date modified: 29/11/2011
 * @author Andrei
 */
public class Offer implements Concept{
    private int price;
    private ArrayList<Order> offerOrders;
    
    public int getPrice(){
        return price;
    }
    
    public ArrayList<Order> getOfferOrders(){
        return offerOrders;
    }
    
    public void setPrice(int price){
        this.price = price;
    }    
    
    public void setOfferOrders(ArrayList<Order> offerOrders){
        this.offerOrders = offerOrders;
    }
}
