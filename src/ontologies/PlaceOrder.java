package ontologies;
import jade.content.AgentAction;
import jade.content.onto.annotations.Slot;

/**Title : Place Bid Order
 * Description: This class implements the agent action ontology for placing Bid Orders
 * Version: 1.0
 * Last date modified: 29/11/2011
 * @author Andrei
 */
public class PlaceOrder implements AgentAction{
    
    private String traderID;
    private String orderID;
    private String side;
    private String orderType;
    private int volume;
    private int price;
    private String status;
   
    public String getTraderID(){
        return traderID;
    }
    
    public int getVolume(){
        return volume;
    }

    @Slot (mandatory = false)
    public int getPice(){
        return price;
    }
    
    public String getOrderID(){
        return orderID;
    }
    
    public String getOrderType(){
        return orderType;
    }
    
    public String getSide(){
        return side;
    }
    
    public String getStatus(){
        return status;
    }
    
    public void setTraderID(String traderID){
        this.traderID = traderID;
    }
    
    public void setOrderID(String orderID){
        this.orderID = orderID;
    }
    
    public void setVolume(int volume){
        this.volume = volume;
    }
    @Slot (mandatory = false)
    public void setPrice(int price){
        this.price = price;
    }
    
    public void setOrderType(String orderType){
        this.orderType = orderType;
    }
    
    public void setSide(String side){
        this.side = side;
    }
    
    public void setStatus(String status){
        this.status = status;
    }
}
