/**Title: Market Vocabulary
 * Description: This interface defines the terminology to be used
 *              by agents in a market
 * Version: 1.0
 * Last date modified: 29/11/2011
 * @author Andrei
 */
package ontologies;

public interface MarketVocabulary {
    //General vocabulary
    public static final int NEW_ORDER = 1;
    public static final int CANCEL_ORDER = 2;
    public static final int GET_SUBMITTED_ORDERS = 3;
    public static final String SERVER_AGENT = "Server Agent";
    
    
}
