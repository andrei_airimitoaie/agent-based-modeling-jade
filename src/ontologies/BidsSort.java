package ontologies;

/**Tite: BidsSort  
 * Description: This class implements a comparator for sorting bids by price in descending order
 * Version: 1.0
 * Last date modified: 22/11/2011
 * @author Andrei
 */
import java.util.Comparator;
 
 
 
public class BidsSort implements Comparator<Bid> 
{
    public int compare(Bid b1, Bid b2)
    {
    	if(b1.getPrice()== b2.getPrice())
    		return 0;
    	else if(b1.getPrice()>b2.getPrice())
    		return -1;
    	else
    		return 1;
 
    }
}
