package ontologies;
import java.util.ArrayList;
import jade.content.Concept;

/**Name: Bid
 * Description: This class will create a bid. The bid will have 2 parameters : Price and a queue of Orders
 * Version: 1.1
 * Last date modified: 29/11/2011
 * @author Andrei
 */
public class Bid implements Concept{
    private int price;
    private ArrayList<Order> bidOrders;
   
    public int getPrice(){
        return price;
    }
    
    public ArrayList<Order> getBidOrders(){
        return bidOrders;
    }
    
    public void setPrice(int price){
        this.price = price;
    }    
    
    public void setBidOrders(ArrayList<Order> bidOrders){
        this.bidOrders = bidOrders;
    }
}
