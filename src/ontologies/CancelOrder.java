
package ontologies;

import jade.content.AgentAction;

/**Title: Cancel Order
 * Description: This class implements the agent action ontology for canceling orders
 * Version: 1.0
 * Last date modified: 04/12/2011
 * @author Andrei
 */
public class CancelOrder implements AgentAction{
    private String orderID;
    
   
    
    public String getOrderID(){
        return orderID;
    }
    
    
    
    public void setOrderID(String orderID){
        this.orderID = orderID;
    }
}
