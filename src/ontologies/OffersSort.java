package ontologies;
/**Title: OfferSort
 * Description: This class implements a comparator for sorting offers in ascending order
 * Version: 1.0
 * Last date modified: 22/11/2011
 * @author Andrei
 */

import java.util.Comparator;
 
 
 
public class OffersSort implements Comparator<Offer> 
{
    public int compare(Offer o1, Offer o2)
    {
    	if(o1.getPrice()== o2.getPrice())
    		return 0;
    	else if(o1.getPrice()<o2.getPrice())
    		return -1;
    	else
    		return 1;
 
    }
}