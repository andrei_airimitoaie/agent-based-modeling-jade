package ontologies;
import jade.content.onto.BeanOntology;
import jade.content.onto.Ontology;
import jade.content.schema.*;
import jade.core.*;


/**Title: Limit Order Book Ontology
 * Description: This class defines the Limit Order Book Ontology
 * Version: 1.0
 * Last date modified: 29/11/2011
 * @author Andrei
 */

public class LimitOrderBookOntology extends BeanOntology{
    
    //The name identifying this ontology
    public static final String ONTOLOGY_NAME = "LimitOrderBookOntology";
    
    //The singleton instance of this ontology
    private static Ontology instance = new LimitOrderBookOntology(ONTOLOGY_NAME);
    
    //Method to access the singleton ontology object
    public static Ontology getInstance(){
        return instance;
    }
    
    //Private constructor
    private LimitOrderBookOntology(String name){
        super(name);
        
        try{
            // ------------- Adding concepts
            add(Bid.class);
            add(Offer.class);
            add(Order.class);
          
            // ------------- Adding actions
            add(PlaceOrder.class);   
       
        }catch (Exception ex) {ex.printStackTrace();}
    }
    
    
}
 