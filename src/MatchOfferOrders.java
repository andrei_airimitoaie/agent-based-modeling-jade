
import java.util.ArrayList;
import ontologies.Bid;
import ontologies.MarketVocabulary;
import ontologies.Order;


/**Title: Matching Offer Orders
 * Description: This class will match offer orders against their counter parties
 * Version: 1.0
 * Last date modified: 05/12/2011
 * @author Andrei
 */
public class MatchOfferOrders implements MarketVocabulary {
    Order offerOrder;
    ArrayList<Bid> bids;
    
    public MatchOfferOrders(Order offerOrder, ArrayList<Bid> bids){
        this.offerOrder = offerOrder;
        this.bids = bids;
    }
    
    public void matchOrders(Order order, ArrayList<Bid> list){
        int price = order.getPrice();
        int volume = order.getVolume();
        int remainingVolume = volume;
        if (list.isEmpty()){
            System.out.println("Couldn't find any matching orders. The order "+ order.getOrderID()+" will remain on the Bids list !");
        }
        else if (list.get(0).getPrice() > price){
            System.out.println("Couldn't find any matching orders. The order "+ order.getOrderID()+" will remain on the Bids list !");
        }
        else{
            for(Bid bid:list){
                while(volume > 0 && !(list.isEmpty())){
                    if((volume - bid.getBidOrders().get(0).getVolume()) >= 0){
                        volume = volume - bid.getBidOrders().get(0).getVolume();
                        remainingVolume = volume;
                        System.out.println(remainingVolume);
                        bid.getBidOrders().remove(0);
                    }
                    else{
                        int volumeLeftInOrder = bid.getBidOrders().get(0).getVolume() - volume;
                        remainingVolume = 0;
                        bid.getBidOrders().get(0).setVolume(volumeLeftInOrder);
                        System.out.println(volumeLeftInOrder);
                        break;
                    }
                }
            }
        }
    }
}
