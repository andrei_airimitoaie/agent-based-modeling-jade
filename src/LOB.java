
import java.util.ArrayList;
import jade.core.*;
import jade.domain.*;
import jade.content.*;
import jade.content.lang.Codec;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.content.onto.basic.Action;
import jade.content.onto.basic.Result;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.SequentialBehaviour;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import java.util.Arrays;
import java.util.Collections;
import ontologies.*;


/**Name: LOB
 * Description: This class will simulate a limit order book, using a linked list 
 *              to store the bids and offers.It will contain lists of type Bid, Offer and a list of rejected market orders.
 * Version: 1.2
 * Last date modified: 29/11/2011
 * @author Andrei
 */
public class LOB extends Agent implements MarketVocabulary {
    
    private static ArrayList<Bid> bidsList = new ArrayList();;
    private static ArrayList<Offer> offersList = new ArrayList();   
    private static ArrayList<PlaceOrder> rejectedOrders;
    
   
    private int orderIDCounter = 1;
    private Codec codec = new SLCodec();
    private Ontology ontology = LimitOrderBookOntology.getInstance();
    
    protected void setup(){
        System.out.println("Order Book initiated");
        
        //Register language and ontology
        getContentManager().registerLanguage(codec);
        getContentManager().registerOntology(ontology);
        
        //Set the main behaviour of the LimitOrderBook
        SequentialBehaviour sb = new SequentialBehaviour();
        sb.addSubBehaviour(new RegisterDF(this));
        sb.addSubBehaviour(new ReceiveMessage(this));
        addBehaviour(sb);
    }   
    
   // Registering the Limit Order Book Service in the yellow pages    
   // for the Trader Agent to be able to retrieve its AID
    private class RegisterDF extends OneShotBehaviour{
        RegisterDF(Agent a){
            super(a);
        }
        
        @Override
        public void action(){
            
            ServiceDescription sd = new ServiceDescription();
            sd.setType(SERVER_AGENT);
            sd.setName(getName());
            DFAgentDescription dfd = new DFAgentDescription();
            dfd.setName(getAID());
            dfd.addServices(sd);
            try{
                DFAgentDescription[] dfds = DFService.search(myAgent, dfd);
                if (dfds.length > 0){
                    DFService.deregister(myAgent, dfd);
                }
                DFService.register(myAgent, dfd);
                System.out.println(getLocalName() + " is ready.");
            }
            catch (Exception ex) {
                System.out.println("Failed registerring with DF! Shutting down");
                ex.printStackTrace();
                doDelete();
            }
        }
    }
    
    // This class receives different types of messages from trading agents
    // and launches appropriate handlers
    
    private class ReceiveMessage extends CyclicBehaviour{
        public ReceiveMessage(Agent a){
            super(a);
        }
        
        @Override
        public void action(){
            ACLMessage msg = receive();
            if (msg == null) {block();return; }
            try {
                Object content = msg.getContentObject();
                if(msg.getPerformative()==ACLMessage.REQUEST){
                    //Verify what type of order the message is
               
                      if (content instanceof PlaceOrder){
                        addBehaviour(new HandleOrder(myAgent,msg));
                      }
                      else if (content instanceof CancelOrder){
                        addBehaviour(new HandleCancelOrder(myAgent,msg));
                      }
                      else {
                        rejectOrder(msg);
                      }
                }
            }catch (Exception ex) {ex.printStackTrace();}
               
        }
    }
    // This class implements the behaviour for adding a bid order
    // to the Limit Order Book
    
    private class HandleOrder extends OneShotBehaviour{
        private ACLMessage request;
        
        HandleOrder(Agent a, ACLMessage request){
            super(a);
            this.request = request;
        }
        
        @Override
        public void action(){
            try{
                PlaceOrder orderAction = (PlaceOrder) request.getContentObject();
                if (orderAction.getSide().equalsIgnoreCase("BUY") || orderAction.getSide().equalsIgnoreCase("SELL")){
                    if (orderAction.getOrderType().equalsIgnoreCase("Limit")){
                         addBehaviour(new HandleLimitOrder(myAgent,request));
                    }
                    else if (orderAction.getOrderType().equalsIgnoreCase("Market")){
                         addBehaviour(new HandleMarketOrder(myAgent,request));
                    }
                }
                else {
                    rejectOrder(request);
                }
            }
            catch(Exception ex) {ex.printStackTrace();}
        }
    }
    
    private class HandleLimitOrder extends OneShotBehaviour{
        private ACLMessage request;
        
        HandleLimitOrder(Agent a, ACLMessage request){
            super(a);
            this.request = request;
        }
        
        @Override
        public void action(){
            try{
                ACLMessage reply = request.createReply();
                PlaceOrder order = (PlaceOrder) request.getContentObject();
                Object obj = processLimitOrder(order);                
                if (obj == null){
                    System.out.println("ERROR !");
                }
                else{
                    reply.setPerformative(ACLMessage.INFORM);
                    Result result = new Result(order, obj);
                    getContentManager().fillContent(reply, result);
                    send(reply);
                }
            }
            catch (Exception ex){ex.printStackTrace();}
        }
    }
    
    Object processLimitOrder(PlaceOrder order){
        String traderID = order.getTraderID();
        String orderID = order.getOrderID();
        String side = order.getSide();
        String type = order.getOrderType();
        int price = order.getPice();
        int volume = order.getVolume();
        Order newOrder = new Order();
        
        if (side.equalsIgnoreCase("BUY")){
            if(price != 0){
                if (getBidsPriceIndex(bidsList,price) == -1){
                    newOrder.setOrderID("B"+Integer.toString(orderIDCounter));
                    newOrder.setTraderID(traderID);
                    newOrder.setSide(side);
                    newOrder.setOrderType(type);
                    newOrder.setPrice(price);
                    newOrder.setVolume(volume);
                    Bid bid = new Bid();
                    bid.setPrice(price);
                    bid.setBidOrders(new ArrayList<Order>(Arrays.asList(newOrder)));
                    bidsList.add(bid);
                    Collections.sort(bidsList, new BidsSort());
                    MatchBidOrders match = new MatchBidOrders(newOrder,offersList);
                    match.matchOrders(newOrder, offersList);
                }
                else{
                    int index = getBidsPriceIndex(bidsList,price);
                    newOrder.setOrderID("B"+Integer.toString(orderIDCounter));
                    newOrder.setTraderID(traderID);
                    newOrder.setSide(side);
                    newOrder.setOrderType(type);
                    newOrder.setPrice(price);
                    newOrder.setVolume(volume);
                    bidsList.get(index).getBidOrders().add(newOrder);
                    MatchBidOrders match = new MatchBidOrders(newOrder,offersList);
                    match.matchOrders(newOrder, offersList);
                    
                }
                 System.out.println("A new bid order, with order ID "+newOrder.getOrderID()+": "+volume+" shares at "+price+" $/share has been placed");
                 orderIDCounter++;
            }
        }
        else if(side.equalsIgnoreCase("SELL")){
             if(price != 0){
                if (getOffersPriceIndex(offersList,price) == -1){
                    newOrder.setOrderID("S"+Integer.toString(orderIDCounter));
                    newOrder.setTraderID(traderID);
                    newOrder.setSide(side);
                    newOrder.setOrderType(type);
                    newOrder.setPrice(price);
                    newOrder.setVolume(volume);
                    Offer offer = new Offer();
                    offer.setPrice(price);
                    offer.setOfferOrders(new ArrayList<Order>(Arrays.asList(newOrder)));
                    offersList.add(offer);
                    Collections.sort(offersList, new OffersSort());
                    MatchOfferOrders match = new MatchOfferOrders(newOrder,bidsList);
                    match.matchOrders(newOrder, bidsList);
                  
                }
                else{
                    int index = getOffersPriceIndex(offersList,price);
                    newOrder.setOrderID("S"+Integer.toString(orderIDCounter));
                    newOrder.setTraderID(traderID);
                    newOrder.setSide(side);
                    newOrder.setOrderType(type);
                    newOrder.setPrice(price);
                    newOrder.setVolume(volume);
                    newOrder.setVolume(volume);
                    offersList.get(index).getOfferOrders().add(newOrder);
                    MatchOfferOrders match = new MatchOfferOrders(newOrder,bidsList);
                    match.matchOrders(newOrder, bidsList);
                }
             
                 System.out.println("A new offer order, with order ID "+newOrder.getOrderID()+": "+volume+" shares at "+price+" $/share has been placed");
                 orderIDCounter++;
            }
        }
         return newOrder;
         
}
    
    private class HandleMarketOrder extends OneShotBehaviour{
        private ACLMessage request;
        
        HandleMarketOrder(Agent a, ACLMessage request){
            super(a);
            this.request = request;
        }
        
        @Override
        public void action(){
             try{
                ContentElement content = getContentManager().extractContent(request);
                PlaceOrder order = (PlaceOrder) request.getContentObject();
                Object obj = processMarketOrder(order);
                ACLMessage reply = request.createReply();
                if (obj == null){
                    System.out.println("ERROR !");
                }
                else{
                    reply.setPerformative(ACLMessage.INFORM);
                    Result result = new Result((Action)content, obj) {};
                    getContentManager().fillContent(reply, result);
                    reply.addReceiver(request.getSender());
                    send(reply);
                }
            }
            catch (Exception ex){ex.printStackTrace();}
        }
    }
    
    Object processMarketOrder(PlaceOrder order){
        String side = order.getSide();
        String traderID = order.getTraderID();
        String type = order.getOrderType();
        int volume = order.getVolume();
        Order newOrder = new Order();
        newOrder.setOrderID("M"+Integer.toString(orderIDCounter));
        orderIDCounter++;
        newOrder.setTraderID(traderID);
        newOrder.setSide(side);
        newOrder.setOrderType(type);
        newOrder.setVolume(volume);
        if (side.equalsIgnoreCase("BUY")){
            matchBuyOrder(order);
        }
        else{
            matchSellOrder(order);
        }
        return newOrder;
    } 
    private class HandleCancelOrder extends OneShotBehaviour{
        private ACLMessage request;
        
        HandleCancelOrder(Agent a, ACLMessage request){
            super(a);
            this.request = request;
        }
        
        @Override
        public void action() {
            try{
               boolean found = false;
               CancelOrder cancel = (CancelOrder) request.getContentObject();
               ACLMessage reply = request.createReply();
               reply.setPerformative(ACLMessage.INFORM);
               
               if(cancel.getOrderID().startsWith("B")){
                   for(Bid bid:bidsList){
                       for (Order order:bid.getBidOrders()){
                           if(cancel.getOrderID().equalsIgnoreCase(order.getOrderID())){
                               bid.getBidOrders().remove(order);
                               reply.setContent(cancel.getOrderID());
                               reply.addReceiver(request.getSender());
                               send(reply);
                               found = true;
                               break;
                           }
                       }
                           
                   }
               }
               else if(cancel.getOrderID().startsWith("S")){
                   for (Offer offer:offersList){
                       for (Order order:offer.getOfferOrders()){
                           if (cancel.getOrderID().equalsIgnoreCase(order.getOrderID())){
                               offer.getOfferOrders().remove(order);
                               reply.setContent(cancel.getOrderID());
                               reply.addReceiver(request.getSender());
                               send(reply);
                               found = true;
                               break;
                           }
                       }
                   }
               }
               if (found == false){rejectOrder(request);}
               else{
                System.out.println("Order with the order id "+cancel.getOrderID()+ "has been canceled!");
               }
        }
            
            catch(Exception ex){ex.printStackTrace();}
        }   
    }
    
    // Method for handling invalid orders
        private void rejectOrder(ACLMessage msg){
        try{
            ACLMessage reply = msg.createReply();
            reply.setPerformative(ACLMessage.NOT_UNDERSTOOD);
            reply.setContent("Operation Rejected");
            reply.addReceiver(msg.getSender());
            send(reply);
            System.out.println("Order Rejected !");
        }
        catch(Exception ex){ex.printStackTrace();}
    } 
    
    
   
    public void matchBuyOrder(PlaceOrder order){
        int volume = order.getVolume();
        int remainingVolume = volume;
        if(offersList.isEmpty()) rejectedOrders.add(order);
        else{
            for(Offer offer:offersList){
                while(volume > 0 && !(offer.getOfferOrders().isEmpty())){
                    if((volume-offer.getOfferOrders().get(0).getVolume()) >= 0){
                        volume = volume - offer.getOfferOrders().get(0).getVolume();
                        remainingVolume = volume;
                        offer.getOfferOrders().remove(0);
                    }
                    else{
                        int volumeLeftInOrder = offer.getOfferOrders().get(0).getVolume() - volume;
                        remainingVolume = 0;
                        offer.getOfferOrders().get(0).setVolume(volumeLeftInOrder);
                        System.out.println(offer.getOfferOrders().get(0).getVolume());
                        break;
                    }
                }
            }
        System.out.println("Market order with order id "+orderIDCounter+ " has been executed ");
        System.out.println("The market order has "+remainingVolume+" shares which haven't been filled");        
        }
    }  
    
     public void matchSellOrder(PlaceOrder order){
        int volume = order.getVolume();
        int remainingVolume = volume;
        if(bidsList.isEmpty()){rejectedOrders.add(order);}
        else{
            for(Bid bid:bidsList){
                while(volume > 0 && !(bid.getBidOrders().isEmpty())){
                    if((volume-bid.getBidOrders().get(0).getVolume()) >= 0){
                        volume = volume - bid.getBidOrders().get(0).getVolume();
                        remainingVolume = volume;
                        bid.getBidOrders().remove(0);
                    }
                    else{
                        int volumeLeftInOrder = bid.getBidOrders().get(0).getVolume() - volume;
                        remainingVolume = 0;
                        bid.getBidOrders().get(0).setVolume(volumeLeftInOrder);
                        System.out.println(bid.getBidOrders().get(0).getVolume());
                        break;
                    }
                }
            }
        System.out.println("Market order with order id "+ orderIDCounter+ " has been executed ");
        System.out.println("The market order has "+remainingVolume+" shares which haven't been filled");
        }
    }
    
        
    public static int getBidsPriceIndex(ArrayList<Bid> bids,int price){
        int index = -1;
        for (Bid bid:bids){
            if (bid.getPrice() == price) index = bids.indexOf(bid);
        }
        return index;
    }
    
    public static int getOffersPriceIndex(ArrayList<Offer> offers, int price){
        int index = -1;
        for (Offer offer:offers){
            if (offer.getPrice() == price) index = offers.indexOf(offer);
        }
        return index;
    }
    
    
      
}
